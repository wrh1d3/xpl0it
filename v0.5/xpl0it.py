#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import sys
import colorama

from core.utils import requirements #check for required modules
from core.utils import logger

if __name__ == "__main__":
	logger.init_history() #load prompt history
	colorama.init() #enable console coloring on Windows
	import core.main #get started!
