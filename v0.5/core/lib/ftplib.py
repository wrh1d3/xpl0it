#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import request

#basic ftp class with proxy use option #wrh1d3 
class FTP:
	def __init__(self, host = None, port = 21, username = None, password = None):
		self.host = host
		self.port = port
		self.username = username
		self.password = password
		self.connected = False
		self.response = ""
		
	def get_response(self):
		return int(self.response[:3]), self.response[4:]
		
	def connect(self):
		self.socket = request.connect_socket(self.host, self.port)
		self.response = self.socket.recv(1024)
		return self.get_response()
		
	def login(self):
		self.response = self.send("USER " + username)
		code, msg = self.get_response()
		if code == 331:
			self.response = self.send("PASS " + password)
			code, msg = self.get_response()
			if code == 230:
				return True
			
	def send(self, s):
		self.socket.send("%s\r\n" %(s)) 
		self.response = self.socket.recv(1024)
		return self.get_response()
		
	def quit(self):
		self.send("QUIT")
		
	def close(self):
		self.socket.disconnect()
