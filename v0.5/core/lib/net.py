#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import mechanize
import cookielib
import ssl
import socks
from pytor import pytor

class Browser(mechanize.Browser):
	def __init__(self):
		mechanize.Browser.__init__(self)
		self.set_handle_robots(False)
		self.set_handle_refresh(False)

	def use_proxy(self, proxy_address, username = None, password = None):
		self.set_proxies({"http":proxy_address})
		if username != None and password != None:
			self.add_proxy_password(username, password)

	def add_header(self, header):
		self.addheaders = [header]

	def use_cookie(self):
		cookie_jar = cookielib.LWPCookieJar()
		self.set_cookiejar(cookie_jar)   
		
class TorBrowser(pytor):
	def __init__(self, host, port):
		pytor.__init__(self, host, port)
		
	def browser(self):
		self.browser = self.mechanizeBrowser()
		self.browser.set_handle_robots(False)
		self.browser.set_handle_refresh(False)
		return self.browser
		
class Sockets:
	def __init__(self, host, port):
		self.host = host
		self.port = port
		self.socket = socks.socksocket()
		
	def check_connection(self):
		try:
			self.connect()
			return True
		except:
			return False
		finally:
			self.disconnect()
		
	def use_http_proxy(self, proxy_host, proxy_port, username = None, password = None):
		self.socket.set_proxy(socks.PROXY_TYPE_HTTP, proxy_host, proxy_port, username, password)
		
	def use_socks5_proxy(self, proxy_host, proxy_port):
		self.socket.set_proxy(socks.PROXY_TYPE_SOCKS5, proxy_host, proxy_port)
		
	def use_ssl(self):
		self.socket = ssl.wrap_socket(self.socket, ssl_version = ssl.PROTOCOL_SSLv23) 
		
	def set_timeout(self, t):
		self.socket.settimeout(t)
		
	def connect(self):
		self.socket.connect((self.host, int(self.port)))

	def send(self, s):
		self.socket.send(str(s))
		
	def recv(self, length):
		return self.socket.recv(length)

	def disconnect(self):
		self.socket.close()
