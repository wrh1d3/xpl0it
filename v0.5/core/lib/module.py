#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

from core.utils import logger

class Base:
	module = ""
	target = ""
	port = ""
	username = ""
	query = ""
	results = ""

	def write_report(self):
		report_datas = {}
		report_datas['module'] = self.module
		if self.target != "":
			report_datas['target'] = self.target
		if self.port != "":
			report_datas['port'] = self.port
		if self.query != "":
			report_datas['query'] = self.query
		report_datas['results'] = self.results
		logger.write_report(report_datas)
		
	def write_log(self, log):
		logger.write_log(log)
