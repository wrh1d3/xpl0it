#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

from core.utils import vars
from colorama import Fore, Back, Style

#set foregorund color
def fore_white(txt, bold = False):
	if not vars.COLORING:
		return txt
	else:
		if bold:
			return Style.BRIGHT + Fore.WHITE + str(txt) + Style.RESET_ALL
		else:
			return Style.NORMAL + Fore.WHITE + str(txt) + Style.RESET_ALL

def fore_yellow(txt, bold = False): #for command line text
	if bold:
		return Style.BRIGHT + Fore.YELLOW + str(txt) + Style.RESET_ALL
	else:
		return Style.NORMAL + Fore.YELLOW + str(txt) + Style.RESET_ALL
	
def fore_green(txt, bold = False):
	if not vars.COLORING:
		return txt
	else:
		if bold:
			return Style.BRIGHT + Fore.GREEN + str(txt) + Style.RESET_ALL
		else:
			return Style.NORMAL + Fore.GREEN + str(txt) + Style.RESET_ALL
	
def fore_red(txt, bold = False):
	if not vars.COLORING:
		return txt
	else:
		if bold:
			return Style.BRIGHT + Fore.RED + str(txt) + Style.RESET_ALL
		else:
			return Style.NORMAL + Fore.RED + str(txt) + Style.RESET_ALL
	
def fore_blue(txt, bold = False):
	if not vars.COLORING:
		return txt
	else:
		if bold:
			return Style.BRIGHT + Fore.BLUE + str(txt) + Style.RESET_ALL
		else:
			return Style.NORMAL + Fore.BLUE + str(txt) + Style.RESET_ALL
	
def fore_magenta(txt, bold = False):
	if not vars.COLORING:
		return txt
	else:
		if bold:
			return Style.BRIGHT + Fore.MAGENTA + str(txt) + Style.RESET_ALL
		else:
			return Style.NORMAL + Fore.MAGENTA + str(txt) + Style.RESET_ALL
	
def fore_cyan(txt, bold = False):
	if not vars.COLORING:
		return txt
	else:
		if bold:
			return Style.BRIGHT + Fore.CYAN + str(txt) + Style.RESET_ALL
		else:
			return Style.NORMAL + Fore.CYAN + str(txt) + Style.RESET_ALL

#set background color
def back_white(txt):
	if not vars.COLORING:
		return txt
	else:
		return Back.WHITE + str(txt) + Style.RESET_ALL

def back_yellow(txt):
	if not vars.COLORING:
		return txt
	else:
		return Back.YELLOW + str(txt) + Style.RESET_ALL

def back_green(txt):
	if not vars.COLORING:
		return txt
	else:
		return Back.GREEN + str(txt) + Style.RESET_ALL

def back_red(txt):
	if not vars.COLORING:
		return txt
	else:
		return Back.RED + str(txt) + Style.RESET_ALL

def back_blue(txt):
	if not vars.COLORING:
		return txt
	else:
		return Back.BLUE + str(txt) + Style.RESET_ALL

def back_magenta(txt):
	if not vars.COLORING:
		return txt
	else:
		return Back.MAGENTA + str(txt) + Style.RESET_ALL

def back_cyan(txt):
	if not vars.COLORING:
		return txt
	else:
		return Back.CYAN + str(txt) + Style.RESET_ALL
