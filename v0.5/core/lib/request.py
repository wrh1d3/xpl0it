#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import random

import net
from core.utils import vars
from core.utils import misc

def connect_http():
	if vars.TOR_PROXY != "" and vars.PROXY_ALIVE:
		tor_browser = net.TorBrowser(vars.PROXY_ADDRESS[0], vars.PROXY_ADDRESS[1])
		browser = tor_browser.browser()
	else:
		browser = net.Browser()
		if vars.HTTP_PROXY != "" and vars.PROXY_ALIVE:
			if vars.PROXY_AUTH != "":
				proxy_auth = misc.parse_string(vars.PROXY_AUTH)
				browser.use_proxy(vars.HTTP_PROXY, proxy_auth[0], proxy_auth[1])
			else:
				browser.use_proxy(vars.HTTP_PROXY)
	if vars.RANDOM_AGENT:
		if vars.AGENTS_LIST != []:
			browser.add_header(("User-agent", random.choice(vars.AGENTS_LIST).strip("\n")))
	if vars.USER_AGENT != "":
		browser.add_header(("User-agent", vars.USER_AGENT))
	if vars.USE_COOKIE:
		browser.use_cookie()
	if vars.HEADERS != "":
		headers = misc.get_headers(vars.HEADERS)
		for header in headers.items():
			browser.add_header(header)
	return browser
	
def connect_socket(host, port):
	socket = net.Sockets(host, port)
	if vars.TOR_PROXY != "" and vars.PROXY_ALIVE:
		socket.use_socks5_proxy(vars.PROXY_ADDRESS[0], vars.PROXY_ADDRESS[1])
	if vars.HTTP_PROXY != "" and vars.PROXY_ALIVE:
		if vars.PROXY_AUTH != "":
			proxy_auth = misc.parse_string(vars.PROXY_AUTH)
			socket.use_http_proxy(vars.PROXY_ADDRESS[0], vars.PROXY_ADDRESS[1], proxy_auth[0], proxy_auth[1])
		else:
			socket.use_http_proxy(vars.PROXY_ADDRESS[0], vars.PROXY_ADDRESS[1])
	if vars.USE_SSL:
		socket.use_ssl()
	socket.set_timeout(vars.TIMEOUT)
	socket.connect()
	return socket
