#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import os
import sys
from PyQt4 import QtGui

import aboutformui

class AboutWindow(QtGui.QDialog, aboutformui.Ui_Dialog):
	def __init__(self):
		super(self.__class__, self).__init__()
		self.setupUi(self)
		
		logo_picture = QtGui.QPixmap(os.getcwd() + "/core/gui/logo.png")
		self.label.setPixmap(logo_picture)
		self.label.show()
		
if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	about_form = AboutWindow()
	about_form.show()
	app.exec_()
