#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations.

import os
import sys
from PyQt4 import QtGui
from PyQt4 import QtCore

import settingsformui
import messagebox

from core.utils import vars
from core.utils import config

class SettingsWindow(QtGui.QDialog, settingsformui.Ui_Dialog):
	def __init__(self):
		super(self.__class__, self).__init__()
		self.setupUi(self)
		
		QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.save)
		QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), self.discard)
		
	def discard(self):
		self.close()
		
	def load(self):
		if not config.load_config():
			messagebox.error_msg("Failed to load configuration file.")
			return
		
		self.spinBox.setValue(vars.VERBOSE)
		self.spinBox_2.setValue(vars.TIMEOUT)
		self.checkBox.setChecked(vars.REPORT)
		self.checkBox_2.setChecked(vars.LOGS)
		self.checkBox_5.setChecked(vars.USE_SSL)
		self.checkBox_4.setChecked(vars.USE_COOKIE)
		self.checkBox_3.setChecked(vars.BATCH_MODE)
		
		if vars.HTTP_PROXY != "":
			self.radioButton.setChecked(True)
			self.groupBox.setChecked(True)
			self.lineEdit.setText(vars.HTTP_PROXY)
				
		if vars.PROXY_AUTH != "":
			self.checkBox_6.setChecked(True)
			self.groupBox.setChecked(True)
			self.lineEdit_2.setText(vars.PROXY_AUTH)
				
		if vars.TOR_PROXY != "":
			self.radioButton_2.setChecked(True)
			self.groupBox.setChecked(True)
			self.lineEdit_3.setText(vars.TOR_PROXY)
			
		if vars.HEADERS != "":
			self.checkBox_7.setChecked(True)
			self.groupBox_2.setChecked(True)
			self.lineEdit_5.setText(vars.HEADERS)
		
		if vars.USER_AGENT != "":
			self.radioButton_3.setChecked(True)
			self.groupBox_2.setChecked(True)
			self.lineEdit_4.setText(vars.USER_AGENT)
			
		self.radioButton_4.setChecked(vars.RANDOM_AGENT)
		if self.radioButton_4.setChecked():
			self.groupBox_2.setChecked(True)
		
	def save(self):
		vars.VERBOSE = int(self.spinBox.value())
		vars.TIMEOUT = int(self.spinBox_2.value())
		vars.REPORT = self.checkBox.isChecked()
		vars.LOGS = self.checkBox_2.isChecked()
		vars.USE_SSL = self.checkBox_5.isChecked()
		vars.USE_COOKIE = self.checkBox_4.isChecked()
		vars.BATCH_MODE = self.checkBox_3.isChecked()
		
		if self.groupBox.isChecked():
			if self.radioButton.isChecked():
				vars.HTTP_PROXY = str(self.lineEdit.text())
			if self.checkBox_6.isChecked():
				vars.PROXY_AUTH = str(self.lineEdit_2.text())
			if self.radioButton_2.isChecked():
				vars.TOR_PROXY = str(self.lineEdit_3.text())

		if self.groupBox_2.isChecked():
			vars.RANDOM_AGENT = self.radioButton_4.isChecked()
			if self.checkBox_7.isChecked():
				vars.HEADERS = str(self.lineEdit_5.text())
			if self.radioButton_3.isChecked():
				vars.USER_AGENT = str(self.lineEdit_4.text())
		
		messagebox.info_msg("Settings saved successfully!")
		self.close()
		
if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	settings_form = SettingsWindow()
	settings_form.show()
	app.exec_()
