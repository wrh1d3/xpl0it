#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

from PyQt4 import QtGui

def _user_choice(choice):
	return choice.text()

def info_msg(text, title = "xpl0it",):
	msg = QtGui.QMessageBox()
	msg.setIcon(QtGui.QMessageBox.Information)
	msg.setText(text)
	msg.setWindowTitle(title)
	msg.setStandardButtons(QtGui.QMessageBox.Ok)
	msg.exec_()
	
def error_msg(text, title = "xpl0it",):
	msg = QtGui.QMessageBox()
	msg.setIcon(QtGui.QMessageBox.Critical)
	msg.setText(text)
	msg.setWindowTitle(title)
	msg.setStandardButtons(QtGui.QMessageBox.Ok)
	msg.exec_()

def warning_msg(text, title = "xpl0it",):
	msg = QtGui.QMessageBox()
	msg.setIcon(QtGui.QMessageBox.Warning)
	msg.setText(text)
	msg.setWindowTitle(title)
	msg.setStandardButtons(QtGui.QMessageBox.Ok)
	msg.exec_()

def question_msg(text, title = "xpl0it",):
	msg = QtGui.QMessageBox()
	msg.setIcon(QtGui.QMessageBox.Question)
	msg.setText(text)
	msg.setWindowTitle(title)
	msg.setStandardButtons(QtGui.QMessageBox.Yes|QtGui.QMessageBox.No)
	msg.connect(_user_choice)
	msg.exec_()
