# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainform.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(559, 417)
        MainWindow.setStatusTip(_fromUtf8(""))
        MainWindow.setDocumentMode(False)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.comboBox = QtGui.QComboBox(self.groupBox)
        self.comboBox.setGeometry(QtCore.QRect(10, 20, 251, 27))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.setItemText(0, _fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.plainTextEdit = QtGui.QPlainTextEdit(self.groupBox)
        self.plainTextEdit.setGeometry(QtCore.QRect(10, 50, 251, 71))
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setPlainText(_fromUtf8(""))
        self.plainTextEdit.setObjectName(_fromUtf8("plainTextEdit"))
        self.groupBox_3 = QtGui.QGroupBox(self.groupBox)
        self.groupBox_3.setGeometry(QtCore.QRect(0, 130, 541, 231))
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.plainTextEdit_2 = QtGui.QPlainTextEdit(self.groupBox_3)
        self.plainTextEdit_2.setGeometry(QtCore.QRect(10, 20, 521, 201))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.plainTextEdit_2.sizePolicy().hasHeightForWidth())
        self.plainTextEdit_2.setSizePolicy(sizePolicy)
        self.plainTextEdit_2.setReadOnly(True)
        self.plainTextEdit_2.setObjectName(_fromUtf8("plainTextEdit_2"))
        self.groupBox_2 = QtGui.QGroupBox(self.groupBox)
        self.groupBox_2.setGeometry(QtCore.QRect(270, 0, 267, 81))
        self.groupBox_2.setCheckable(False)
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.lineEdit = QtGui.QLineEdit(self.groupBox_2)
        self.lineEdit.setGeometry(QtCore.QRect(10, 20, 191, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.lineEdit_2 = QtGui.QLineEdit(self.groupBox_2)
        self.lineEdit_2.setGeometry(QtCore.QRect(10, 50, 251, 27))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.spinBox = QtGui.QSpinBox(self.groupBox_2)
        self.spinBox.setGeometry(QtCore.QRect(210, 20, 48, 27))
        self.spinBox.setToolTip(_fromUtf8(""))
        self.spinBox.setMinimum(1)
        self.spinBox.setMaximum(65535)
        self.spinBox.setProperty("value", 80)
        self.spinBox.setObjectName(_fromUtf8("spinBox"))
        self.pushButton = QtGui.QPushButton(self.groupBox)
        self.pushButton.setGeometry(QtCore.QRect(280, 90, 101, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.groupBox_2.raise_()
        self.comboBox.raise_()
        self.plainTextEdit.raise_()
        self.groupBox_3.raise_()
        self.pushButton.raise_()
        self.gridLayout.addWidget(self.groupBox, 2, 2, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 559, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setSizeGripEnabled(True)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionInstall = QtGui.QAction(MainWindow)
        self.actionInstall.setObjectName(_fromUtf8("actionInstall"))
        self.actionPurge_output = QtGui.QAction(MainWindow)
        self.actionPurge_output.setObjectName(_fromUtf8("actionPurge_output"))
        self.actionQuit = QtGui.QAction(MainWindow)
        self.actionQuit.setObjectName(_fromUtf8("actionQuit"))
        self.actionSettings = QtGui.QAction(MainWindow)
        self.actionSettings.setObjectName(_fromUtf8("actionSettings"))
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setObjectName(_fromUtf8("actionAbout"))
        self.actionSettings_2 = QtGui.QAction(MainWindow)
        self.actionSettings_2.setObjectName(_fromUtf8("actionSettings_2"))
        self.actionAbout_2 = QtGui.QAction(MainWindow)
        self.actionAbout_2.setObjectName(_fromUtf8("actionAbout_2"))
        self.actionQuit_2 = QtGui.QAction(MainWindow)
        self.actionQuit_2.setObjectName(_fromUtf8("actionQuit_2"))
        self.actionDonate = QtGui.QAction(MainWindow)
        self.actionDonate.setObjectName(_fromUtf8("actionDonate"))
        self.menuFile.addSeparator()
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionSettings_2)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionDonate)
        self.menuFile.addAction(self.actionAbout_2)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionQuit_2)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "xpl0it Web Penetration Testing Framework (GUI)", None))
        self.groupBox.setTitle(_translate("MainWindow", "Modules:", None))
        self.comboBox.setItemText(1, _translate("MainWindow", "hostinfos", None))
        self.comboBox.setItemText(2, _translate("MainWindow", "banner", None))
        self.comboBox.setItemText(3, _translate("MainWindow", "google", None))
        self.comboBox.setItemText(4, _translate("MainWindow", "shodanvuln", None))
        self.comboBox.setItemText(5, _translate("MainWindow", "portscan", None))
        self.comboBox.setItemText(6, _translate("MainWindow", "sqli", None))
        self.comboBox.setItemText(7, _translate("MainWindow", "ftpfuzz", None))
        self.comboBox.setItemText(8, _translate("MainWindow", "ftpbrute", None))
        self.comboBox.setItemText(9, _translate("MainWindow", "smtpbrute", None))
        self.comboBox.setItemText(10, _translate("MainWindow", "httpbrute", None))
        self.groupBox_3.setTitle(_translate("MainWindow", "Output:", None))
        self.groupBox_2.setTitle(_translate("MainWindow", "Options:", None))
        self.lineEdit.setText(_translate("MainWindow", "Target hostname or IP address", None))
        self.lineEdit_2.setText(_translate("MainWindow", "Username", None))
        self.pushButton.setText(_translate("MainWindow", "Run module", None))
        self.menuFile.setTitle(_translate("MainWindow", "Menu", None))
        self.actionInstall.setText(_translate("MainWindow", "Install xpl0it", None))
        self.actionPurge_output.setText(_translate("MainWindow", "Purge output ", None))
        self.actionQuit.setText(_translate("MainWindow", "Quit", None))
        self.actionQuit.setShortcut(_translate("MainWindow", "Alt+Q", None))
        self.actionSettings.setText(_translate("MainWindow", "Settings", None))
        self.actionSettings.setShortcut(_translate("MainWindow", "Alt+S", None))
        self.actionAbout.setText(_translate("MainWindow", "About", None))
        self.actionAbout.setShortcut(_translate("MainWindow", "Alt+A", None))
        self.actionSettings_2.setText(_translate("MainWindow", "Settings", None))
        self.actionSettings_2.setShortcut(_translate("MainWindow", "Ctrl+S", None))
        self.actionAbout_2.setText(_translate("MainWindow", "About", None))
        self.actionQuit_2.setText(_translate("MainWindow", "Quit", None))
        self.actionQuit_2.setShortcut(_translate("MainWindow", "Ctrl+Q", None))
        self.actionDonate.setText(_translate("MainWindow", "Donate (Bitcoin)", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

