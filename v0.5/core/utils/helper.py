#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations.

import sys
from terminaltables import SingleTable

from core.lib import colors
import stdout

from core.modules.discoverers import hostinfos
from core.modules.gatherers import google
from core.modules.gatherers import banner
from core.modules.gatherers import shodanvuln
from core.modules.scanners import portscan
from core.modules.scanners import sqli
from core.modules.fuzzers import ftpfuzz
from core.modules.bruters import ftpbrute
from core.modules.bruters import smtpbrute
from core.modules.bruters import httpbrute

def commands_help():
	print colors.fore_cyan("help") + "\t\tShow this help message."
	print colors.fore_cyan("modules") + "\t\tList all modules."
	print colors.fore_cyan("load <module>") + "\tLoad module by name."
	print colors.fore_cyan("sh <command>") + "\tExecute shell command."
	print colors.fore_cyan("exit") + "\t\tExit application."

def modules_list():
	table_datas = [[colors.fore_cyan("Module", True), colors.fore_cyan("Brief description", True)]]
	table_datas.append([colors.fore_cyan(hostinfos.MODULE_NAME), hostinfos.DESCRIPTION])
	table_datas.append([colors.fore_cyan(banner.MODULE_NAME), banner.DESCRIPTION])
	table_datas.append([colors.fore_cyan(google.MODULE_NAME), google.DESCRIPTION])
	table_datas.append([colors.fore_cyan(shodanvuln.MODULE_NAME), shodanvuln.DESCRIPTION])
	table_datas.append([colors.fore_cyan(portscan.MODULE_NAME), portscan.DESCRIPTION])
	table_datas.append([colors.fore_cyan(sqli.MODULE_NAME), sqli.DESCRIPTION])
	table_datas.append([colors.fore_cyan(ftpfuzz.MODULE_NAME), ftpfuzz.DESCRIPTION])
	table_datas.append([colors.fore_cyan(ftpbrute.MODULE_NAME), ftpbrute.DESCRIPTION])
	table_datas.append([colors.fore_cyan(smtpbrute.MODULE_NAME), smtpbrute.DESCRIPTION])
	table_datas.append([colors.fore_cyan(httpbrute.MODULE_NAME), httpbrute.DESCRIPTION])
	print SingleTable(table_datas, "").table

def load_module(module):
	if module == "hostinfos":
		hostinfos.HostInfos()
	elif module == "banner":
		banner.BannerGrabber()
	elif module == "google":
		google.GoogleSearch()
	elif module == "shodanvuln":
		shodanvuln.ShodanSearch()
	elif module == "portscan":
		portscan.PortScanner()
	elif module == "sqli":
		sqli.SQLScanner()
	elif module == "ftpfuzz":
		ftpfuzz.FTPFuzz()
	elif module == "ftpbrute":
		ftpbrute.FTPBrute()
	elif module == "smtpbrute":
		smtpbrute.SMTPBrute()
	elif module == "httpbrute":
		httpbrute.HTTPBrute()
	else:
		print colors.back_red("Unkown module. Type 'modules' for modules list.")
