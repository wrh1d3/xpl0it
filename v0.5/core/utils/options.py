#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import os
from optparse import OptionGroup, OptionParser

import vars

parser = OptionParser(usage = "python %prog [Options]")

#general options
general = OptionGroup(parser, "\033[1;4mGeneral\033[0m", "These options relate to general stuff.")
general.add_option("--banner", dest = "banner", action = "store_true", 
	help = "Show application's banner and exit.")
general.add_option("--verbose", dest = "verbose", default = str(vars.VERBOSE), type = "int", action = "store", 
	help = "Set verbosity level: 0-1 (default " + str(vars.VERBOSE) + ").")
general.add_option("--report", dest = "report", default = vars.REPORT, action = "store_true", 
	help = "Generate session report.")
general.add_option("--logs", dest = "logs", default = vars.LOGS, action = "store_true", 
	help = "Save logs to file.")
general.add_option("--batch", dest = "batch", default = vars.BATCH_MODE, action = "store_true", 
	help = "Never ask for user input, use the default behaviour.")
general.add_option("--config", dest = "config", action = "store_true", 
	help = "Use configuration file.")
general.add_option("--target", dest = "target", default = vars.TOR_PROXY, action = "store", 
	help = "Specify target IP address or Hostname.")
general.add_option("--disable-coloring", dest = "disable_coloring", default = False, action = "store_true", 
	help = "Disable console output coloring.")

#request options
request = OptionGroup(parser, "\033[1;4mRequest\033[0m", "These options can be used to specify how to connect to the target.")
request.add_option("--cookie", dest = "cookie", default = vars.USE_COOKIE, action = "store_true", 
	help = "Use cookie when emulating browser.")
request.add_option("--ssl", dest = "ssl", default = vars.USE_SSL, action = "store_true", 
	help = "Force use of SSL/HTTPS.")
request.add_option("--http-proxy", dest = "http_proxy", default = vars.HTTP_PROXY, action = "store",
	help = "Connect thru HTTP proxy (e.g: 127.0.0.1:80).")
request.add_option("--tor-proxy", dest = "tor_proxy", default = vars.TOR_PROXY, action = "store", 
	help = "Connect thru Tor Socks5 proxy (e.g: 127.0.0.1:9050).")
request.add_option("--proxy-auth", dest = "proxy_auth", default = vars.PROXY_AUTH, action = "store",
	help = "Set HTTP proxy authentification credentials (e.g: user:password).")
request.add_option("--user-agent", dest = "user_agent", default = vars.USER_AGENT, action = "store", 
	help = "Set custom HTTP user-agent.")
request.add_option("--random-agent", dest = "random_agent", default = vars.RANDOM_AGENT, action = "store_true", 
	help = "Use random HTTP user-agent.")
request.add_option("--headers", dest = "headers", default = vars.HEADERS, action = "store", 
	help = "Add custom HTTP headers (e.g: Connection:keep-alive;Accept-Encoding:gzip,deflate,sdch;...).")
request.add_option("--timeout", dest = "timeout", default = str(vars.TIMEOUT), type = "int", action = "store",
	help = "Set socket connection timeout (default " + str(vars.TIMEOUT) + ").")

#miscellaneous options
miscs = OptionGroup(parser, "\033[1;4mMiscellaneous\033[0m")
miscs.add_option("--install", dest = "install", action = "store_true", 
	help = "Install xpl0it on system.")
miscs.add_option("--purge-output", dest = "purge_output", action = "store_true", 
	help = "Safely remove all content of output directory.")
	
parser.add_option_group(general)
parser.add_option_group(request)
parser.add_option_group(miscs)
	
#from sqlmap, display longer options without breaking into two lines
def _(self, *args):
	_ = parser.formatter._format_option_strings(*args)
	if len(_) > 18:
		_ = ("%%.%ds.." % (18 - parser.formatter.indent_increment)) % _
	return _

parser.formatter._format_option_strings = parser.formatter.format_option_strings
parser.formatter.format_option_strings = type(parser.formatter.format_option_strings)(_, parser, type(parser))

option = parser.get_option("-h")
option.help = option.help.replace("show this help message and exit", "Show this help message and exit.")

#set options
(options, args) = parser.parse_args()
