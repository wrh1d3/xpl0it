#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations.

import os
import sys
import platform
from subprocess import call

import vars
import misc
import stdout

bin_path = vars.SHARE_PATH + "/" + vars.PROGRAM + ".py"
gui_bin_path = vars.SHARE_PATH + "/" + vars.PROGRAM_GUI + ".py"

def bin_script(path):
	source = """#!/bin/sh

	#This file is a part of xpl0it Web Penetration Testing Framework
	#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

	#This program is free, you can redistribute it and/or modify
	#it according to respect of it's original author.

	#See 'README.txt' for more informations.

	python %s "$@" """ %(path)
	return source

def uninstall():
	try:
		call("rm -f " + vars.BIN_PATH, shell = True)
		call("rm -rdf " + vars.SHARE_PATH, shell = True)
	except Exception, ex_msg:
		critical_msg = str(ex_msg)
		print stdout.critical_msg(critical_msg)

def install():
	if platform.system() != "Linux": #sorry windows users :p
		critical_msg = "The installer is designed only for Linux based distributions."
		critical_msg += " This system is not yet supported."
		print stdout.critical_msg(critical_msg)
		return
	if os.geteuid() != 0:
		critical_msg = "This action requires root privileges."
		print stdout.critical_msg(critical_msg)
		return
	if os.path.exists(vars.SHARE_PATH) or os.path.exists(vars.BIN_PATH):
		warning_msg = "It seems that " + vars.PROGRAM
		warning_msg += " is already installed on this system."
		print stdout.warning_msg(warning_msg)
		
		input = ""
		if vars.BATCH_MODE:
			input = "Y"
		else:
			while True:
				question_msg = "Do you want to uninstall xpl0it (Y/n) "
				input = raw_input(stdout.question_msg(question_msg))
				if input in vars.YES_CHOICE or input in vars.NO_CHOICE:
					break
		if input in vars.YES_CHOICE:
			uninstall()
		return
	try:	
		call("cp -r " + os.getcwd() + " " + vars.SHARE_PATH, shell = True)
		call("chmod 775 " + bin_path, shell = True)
		
		script = bin_script(bin_path)
		misc.write_file(vars.BIN_PATH, script, "w")
		script = bin_script(gui_bin_path)
		misc.write_file(vars.BIN_PATH, script, "w")
		
		call("chmod 555 " + vars.BIN_PATH, shell = True)
		
		success_msg = "Installation finished successfully!"
		success_msg += " Type '" + vars.PROGRAM + "' or '" + vars.PROGRAM_GUI + "' in terminal to launch it."
		print stdout.success_msg(success_msg)
	except Exception, ex_msg:
		critical_msg = str(ex_msg)
		print stdout.critical_msg(critical_msg)
