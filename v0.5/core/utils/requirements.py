#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations.

import sys
import platform
from subprocess import call

import stdout
import vars

def check_requirements():
	try:
		if platform.system() == "Windows":
			import BeautifulSoup
		else:
			from bs4 import BeautifulSoup
		import mechanize
		import colorama
		import terminaltables
		import stem
		import socks
		import pygeoip
		import scapy
		import readline
		import shodan
		#check also missing modules if I forget :P
		return True
	except ImportError:
		return False

if not check_requirements():
	question_msg = "It seems that some required modules are missing."
	question_msg += " Do you want to process modules download (y/n) "
	input = raw_input(stdout.question_msg(question_msg))
	if input not in vars.YES_CHOICE:
		sys.exit(0)
	else:
		info_msg = "Processing modules download, please wait..."
		print stdout.info_msg(info_msg)
		try:
			#cross platforms modules
			call("pip install BeautifulSoup mechanize colorama terminaltables stem" \
				" pygeoip scapy shodan", shell = True)
			
			if platform.system() == "Windows": #thanks to AlfredTH :)
				call("pip install SocksiPy-branch pyreadline", shell = True)
			else:
				call("pip install SocksiPy readline", shell = True)
		except Exception, ex_msg:
			critical_msg = str(ex_msg)
			print stdout.critical_msg(critical_msg)
			sys.exit(0)
