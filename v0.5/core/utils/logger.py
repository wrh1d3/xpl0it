#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import os
import time
import readline
import atexit

import misc
import vars

def init_history():
	history_file = os.path.expanduser("__history__")
	if os.path.exists(history_file) == True: 
		readline.read_history_file(history_file) #read prompt history
	atexit.register(readline.write_history_file, history_file) #save prompt history at exit
	
def write_log(log):
	logs_file = vars.LOGS_FOLDER + "/logs_%s.txt" %(time.strftime("%d%m%Y"))
	misc.check_file(logs_file, "[" + time.strftime("%H:%M:%S") + "] Started\n")
	misc.write_file(logs_file, "[" + time.strftime("%H:%M:%S") + "] " + log + "\n")
	
def write_report(report_datas):
	if report_datas.has_key("target"):
		report_folder = vars.REPORT_FOLDER + "/" + report_datas['target']
		if not os.path.exists(report_folder):
			os.makedirs(report_folder)
	else:
		report_folder = vars.REPORT_FOLDER
			
	report_file = report_folder + "/report_%s.txt" %(time.strftime("%d%m%Y"))
	misc.check_file(report_file, "%s %s > REPORT FILE\n\n" %(vars.PROGRAM, vars.DESCRIPTION))
	misc.write_file(report_file, "[*] Module: " + report_datas['module'] + "\n")
	
	if report_datas.has_key("target"):
		misc.write_file(report_file, "[*] Target: " + report_datas['target'] + "\n")
	if report_datas.has_key("ports"):
		misc.write_file(report_file, "[*] Port(s): " + report_datas['ports'] + "\n")
	if report_datas.has_key("query"):
		misc.write_file(report_file, "[*] Query: " + report_datas['query'] + "\n")
	misc.write_file(report_file, "\n" + report_datas['results'] + "\n")
