#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import os

def check_url(url):
	if (not url.startswith("http://")) or (not url.startswith("https://")):
		return "http://" + url #add http:// tag for mechanize browser
		
def parse_string(s, delimiter = ":"):
	s = s.split(delimiter)
	return s
	
def parse_args(s):
	arg = ""
	for args in s:
		arg += args + " "  
	return args

def parse_reply(msg):
	return int(msg[:3]), msg[4:]
	
def write_file(filename, s, mode = "a"):
	f = open(filename, mode)
	f.writelines(s)
	f.close()

def read_file(filename):
	lines = []
	if os.path.exists(filename):
		f = open(filename, "r")
		lines = f.readlines()
		f.close()
	return lines

def check_file(filename, s):
	if not os.path.exists(filename):
		write_file(filename, s, "w")

def get_headers(headers):
	_headers = {}
	for header in headers.split(";"):
		items = header.split(":")
		_headers[items[0]] = items[1]
	return _headers
