#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import os
import vars
import misc

def load_config():
	if not os.path.exists(vars.CONFIG_FILE):
		return False
		
	config = misc.read_file(vars.CONFIG_FILE)
	for cfg in config:
		cfg = cfg.strip("\n")
		s = misc.parse_string(cfg, "=")
		
		if s[0] == "MODULE":
			vars.MODULE = s[1]
		elif s[0] == "TARGET":
			vars.TARGET = s[1]
		elif s[0] == "PORT":
			vars.PORT = s[1]
		elif s[0] == "QUERY":
			vars.QUERY = s[1]
		elif s[0] == "USERNAME":
			vars.USERNAME = s[1]
		elif s[0] == "MAX_RESULTS":
			vars.MAX_RESULTS = s[1]
		elif s[0] == "API_KEY":
			vars.API_KEY = s[1]
		elif s[0] == "TIMEOUT":
			vars.TIMEOUT = int(s[1])
		elif s[0] == "VERBOSE":
			vars.VERBOSE = int(s[1])
		elif s[0] == "HTTP_PROXY":
			vars.HTTP_PROXY = s[1]
		elif s[0] == "TOR_PROXY":
			vars.TOR_PROXY = s[1]
		elif s[0] == "PROXY_AUTH":
			vars.PROXY_AUTH = s[1]
		elif s[0] == "HEADERS":
			vars.HEADERS = s[1]
		elif s[0] == "USER_AGENT":
			vars.USER_AGENT = s[1]
		elif s[0] == "LOGS":
			vars.LOGS = bool(int(s[1]))
		elif s[0] == "REPORT":
			vars.REPORT = bool(int(s[1]))
		elif s[0] == "USE_SSL":
			vars.USE_SSL = bool(int(s[1]))
		elif s[0] == "USE_COOKIE":
			vars.USE_COOKIE = bool(int(s[1]))
		elif s[0] == "RANDOM_AGENT":
			vars.RANDOM_AGENT = bool(int(s[1]))
		elif s[0] == "BATCH_MODE":
			vars.BATCH_MODE = bool(int(s[1]))
		elif s[0] == "PROXY_ALIVE":
			vars.PROXY_ALIVE = bool(int(s[1]))
		elif s[0] == "GUI_MODE":
			vars.GUI_MODE = bool(int(s[1]))
	return True

def save_config():
	config = "MODULE=" + vars.MODULE + "\nTARGET=" + vars.TARGET + "\nPORT=" + vars.PORT + \
		"\nQUERY=" + vars.QUERY + "\nUSERNAME=" + vars.USERNAME + "\nMAX_RESULTS=" + \
		vars.MAX_RESULTS + "\nAPI_KEY=" + vars.API_KEY + "\nTIMEOUT=" + str(vars.TIMEOUT) + \
		"\nVERBOSE=" + str(vars.VERBOSE) + "\nHTTP_PROXY=" + vars.HTTP_PROXY + "\nTOR_PROXY=" + \
		vars.TOR_PROXY + "\nPROXY_AUTH=" + vars.PROXY_AUTH + "\nHEADERS=" + vars.HEADERS + \
		"\nUSER_AGENT=" + str(vars.USER_AGENT) + "\nLOGS=" + str(int(vars.LOGS)) + "\nREPORT=" + \
		str(int(vars.REPORT)) + "\nUSE_SSL=" + str(int(vars.USE_SSL)) + "\nUSE_COOKIE=" + \
		str(int(vars.USE_COOKIE)) + "\nRANDOM_AGENT=" + str(int(vars.RANDOM_AGENT)) + \
		"\nBATCH_MODE=" + str(int(vars.BATCH_MODE)) + "\nPROXY_ALIVE=" + \
		str(int(vars.PROXY_ALIVE)) + "\nGUI_MODE=" + str(int(vars.GUI_MODE))
	misc.write_file(vars.CONFIG_FILE, config, "w")
