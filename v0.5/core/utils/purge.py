#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import os
import sys
import shutil
import random

import vars
import stdout
import logger

#from sqlmap
def purge_output():
	filepaths = []
	dirpaths = []
	
	for rootpath, directories, filenames in os.walk(vars.OUTPUT_FOLDER):
		dirpaths.extend([os.path.abspath(os.path.join(rootpath, _)) for _ in directories])
		filepaths.extend([os.path.abspath(os.path.join(rootpath, _)) for _ in filenames])

	#changing file attributes
	for filepath in filepaths:
		try:
			os.chmod(filepath, stat.S_IREAD | stat.S_IWRITE)
		except:
			pass
	
	#writing random data to files
	for filepath in filepaths:
		try:
			filesize = os.path.getsize(filepath)
			with open(filepath, "w+b") as f:
				f.write("".join(chr(random.randint(0, 255)) for _ in xrange(filesize)))
		except:
			pass

	#truncating files
	for filepath in filepaths:
		try:
			with open(filepath, 'w') as f:
				pass
		except:
			pass

	#renaming filenames to random values
	for filepath in filepaths:
		try:
			os.rename(filepath, os.path.join(os.path.dirname(filepath), "".join(random.sample(string.ascii_letters, random.randint(4, 8)))))
		except:
			pass

	dirpaths.sort(cmp=lambda x, y: y.count(os.path.sep) - x.count(os.path.sep))

	#renaming directory names to random values
	for dirpath in dirpaths:
		try:
			os.rename(dirpath, os.path.join(os.path.dirname(dirpath), "".join(random.sample(string.ascii_letters, random.randint(4, 8)))))
		except:
			pass

	#deleting the whole directory tree
	os.chdir(os.path.join(vars.OUTPUT_FOLDER, ".."))

	try:
		shutil.rmtree(vars.OUTPUT_FOLDER)
		print stdout.success("Done!")
	except Exception, ex_msg:
		critical_msg = str(ex_msg)
		print stdout.critical_msg(critical_msg)
		if vars.LOGS:
			logger.write_log(critical_msg)
