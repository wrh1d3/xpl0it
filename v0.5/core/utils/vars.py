#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import os

#program infos
PROGRAM = "xpl0it"
PROGRAM_GUI = "xpl0itGui"
VERSION = "0.5#dev"
AUTHOR = "wrh1d3"
CONTRIBUTORS = "AlfredTH (alfredth.com);" #thanks for windows implementation (requirements.py)
CONTACTS = "wrh1d3@gmail.com"
DESCRIPTION = "Web Penetration Testing Framework"
COPYRIGHT = "2016-2018 J3kill Soft."

#banner
BANNER = """\033[1;31m                         __
 __  _	____  __   ____ /|_| __     \033[0;1;4;36m%s\033[0m\033[1;31m
/\ \/ \/|   |/| | /|   |/| |/|  >   \033[0;36mby %s\033[31m\033[1m
\/    /||  _||| |_|| \033[37m0\033[31m\033[1m ||| ||| |_
/\_/\_\||_|_/||___||___|||_|||___|  \033[1;33m%s\033[31m\033[1m
\/_/\_/|/_/  |/__/|/___/|/_/|/__/\033[0m 
""" %(VERSION, AUTHOR, COPYRIGHT)

#main variables
MODULE = ""
TARGET = ""
PORT = ""
USERNAME = ""
QUERY = ""
MAX_RESULTS = ""
API_KEY = ""
HTTP_PROXY = ""
TOR_PROXY = ""
PROXY_AUTH = ""
HEADERS = ""
USER_AGENT = ""
TIMEOUT = 1
VERBOSE = 1
PROXY_ADDRESS = []
AGENTS_LIST = []
GUI_MODE = False
LOGS = False
REPORT = False
USE_SSL = False
USE_COOKIE = False
RANDOM_AGENT = False
BATCH_MODE = False
PROXY_ALIVE = False
COLORING = True

#choice variables
YES_CHOICE = ["y", "Y", "yes", "Yes", "YES"]
NO_CHOICE = ["n", "N", "no", "No", "NO"]

#dir and files variables
SHARE_PATH = "/usr/share/" + PROGRAM
BIN_PATH = "/usr/bin/" + PROGRAM
OUTPUT_FOLDER = os.getcwd() + "/output"
REPORT_FOLDER = OUTPUT_FOLDER + "/reports"
LOGS_FOLDER = OUTPUT_FOLDER + "/logs"
AGENTS_FILE = os.getcwd() + "/core/data/agents.txt"
WORDLIST = os.getcwd() + "/core/data/wordlist.txt"
GEOIP_FILE  = os.getcwd() + "/core/data/geoip.dat"
CONFIG_FILE = os.getcwd() + "/core/data/config" 
