#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import vars
from core.lib import colors

def content_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return "[" + colors.fore_cyan("~", True) + "] " + str(msg)
		else:
			return "[~] " + str(msg)
	else:
		return "[~] " + str(msg)

def info_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return "[" + colors.fore_blue("*", True) + "] " + str(msg)
		else:
			return "[*] " + str(msg)
	else:
		return "[*] " + str(msg)

def error_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return "[" + colors.fore_red("x") + "] " + colors.fore_red("Error: ") + str(msg)
		else:
			return "[x] Error: " + str(msg)
	else:
		return "[x] Error: " + str(msg)

def warning_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return "[" + colors.fore_yellow("!") + "] " + colors.fore_yellow("Warning: ") + str(msg)
		else:
			return "[!] Warning: " + str(msg)
	else:
		return "[!] Warning: " + str(msg)
	
def critical_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return colors.back_red("[x] Critical: " + str(msg))
		else:
			return "[x] Critical: " + str(msg)
	else:
		return "[x] Critical: " + str(msg)
	
def success_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return "[" + colors.fore_green("+", True) + "] " + str(msg)
		else:
			return "[+] " + str(msg)
	else:
		return "[+] " + str(msg)

def failed_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return "[" + colors.fore_red("-", True) + "] " + str(msg)
		else:
			return "[-] " + str(msg)
	else:
		return "[-] " + str(msg)

def checking_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return "[" + colors.fore_magenta("...", True) + "] " + str(msg)
		else:
			return "[...] " + str(msg)
	else:
		return "[...] " + str(msg)

def question_msg(msg):
	if vars.GUI_MODE == False:
		if vars.COLORING:
			return "[" + colors.fore_blue("?", True) + "] " + colors.fore_blue(str(msg), True)
		else:
			return "[?] " + str(msg)
	else:
		return "[?] " + str(msg)
