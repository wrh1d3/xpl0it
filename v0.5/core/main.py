#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations.

import os
import sys
from colorama import Style

from lib import net
from lib import colors

from utils import helper
from utils import vars
from utils import misc
from utils import stdout
from utils import install
from utils import purge
from utils import config

from utils.options import *

#check command line arguments
if options.config:
	if not config.load_config():
		critical_msg = "Configuration file '%s' not found." %(vars.CONIFG_FILE)
		print stdout.critical_msg(critical_msg)
		sys.exit(0)
else:
	#welcome message
	print vars.BANNER 
	print "\033[0;1;4;33mAuthor:\033[0m\033[1;31m" + colors.fore_cyan(" %s (%s)", True) %(vars.AUTHOR, vars.CONTACTS)
	print "\033[0;1;4;33mContributors:\033[0m\033[1;31m" + colors.fore_cyan(" %s\n", True) %(vars.CONTRIBUTORS)
	
	#set to console mode
	vars.GUI_MODE = False
	
	if options.banner:
		sys.exit(0)

	if options.http_proxy != "" and options.tor_proxy != "":
		critical_msg = "Tor Socks5 '--tor-proxy' and HTTP proxy '--http-proxy'"
		critical_msg += " can not be used simultaneously. Please choose only one option."
		print stdout.critical_msg(critical_msg)
		sys.exit(0)
		
	if options.user_agent != "" and options.random_agent != "":
		critical_msg = "Custom HTTP user-agent '--user-agent' and random HTTP user-agent '--random-agent'"
		critical_msg += " can not be used simultaneously. Please choose only one option."
		print stdout.critical_msg(critical_msg)
		sys.exit(0)
		
	if options.install:
		info_msg = "Processing " + vars.PROGRAM + " installation, please wait..."
		print stdout.info_msg(info_msg)
		install.install()
		sys.exit(0)
		
	if options.purge_output:
		if os.path.exists(vars.OUTPUT_FOLDER):
			purge.purge_output()
		else:
			error_msg = "Skip output directory content purge."
			error_msg += " Directory '" + vars.OUTPUT_FOLDER + "' does not exists."
			print stdout.error_msg(error_msg)

	if not os.path.exists(vars.OUTPUT_FOLDER):
		os.makedirs(vars.OUTPUT_FOLDER) #create output folder
		
	if options.disable_coloring:
		warning_msg = "Console ouput coloring is disable."
		warning_msg += " You are going to miss some beautifull effects."
		print stdout.warning_msg(warning_msg)
		
	#set config
	vars.TARGET = options.target
	vars.VERBOSE = options.verbose
	vars.TIMEOUT = options.timeout
	vars.REPORT = options.report
	vars.LOGS = options.logs
	vars.USE_SSL = options.ssl
	vars.USE_COOKIE = options.cookie
	vars.USER_AGENT = options.user_agent
	vars.RANDOM_AGENT = options.random_agent
	vars.BATCH_MODE = options.batch
	vars.HTTP_PROXY = options.http_proxy
	vars.PROXY_AUTH = options.proxy_auth
	vars.TOR_PROXY = options.tor_proxy
	vars.HEADERS = options.headers
	vars.COLORING = not options.disable_coloring
	
#check config
if vars.VERBOSE not in range(1): #must be in range 0 or 1
	vars.VERBOSE = 1
		
if vars.TIMEOUT < 1: #must be more than or equal to 1
	vars.TIMEOUT = 1
		
if vars.REPORT:
	if not os.path.exists(vars.REPORT_FOLDER):
		os.makedirs(vars.REPORT_FOLDER)

if vars.LOGS:
	if not os.path.exists(vars.LOGS_FOLDER):
		os.makedirs(vars.LOGS_FOLDER)

if vars.RANDOM_AGENT:
	vars.AGENTS_LIST = misc.read_file(vars.AGENTS_FILE)
	if vars.AGENTS_LIST == []:
		warning_msg = "User-agents file '" + vars.AGENTS_FILE + "' not found."
		print stdout.warning_msg(warning_msg)
		
if vars.TOR_PROXY != "":
	vars.PROXY_ADDRESS = misc.parse_string(vars.TOR_PROXY)
elif vars.HTTP_PROXY != "":
	vars.PROXY_ADDRESS = misc.parse_string(vars.HTTP_PROXY)
		
if vars.PROXY_ADDRESS != []:
	try:
		socket = net.Sockets(vars.PROXY_ADDRESS[0], vars.PROXY_ADDRESS[1])
		socket.connect()
		socket.disconnect()
	except Exception:
		warning_msg = "Proxy server address '" + vars.PROXY_ADDRESS[0]
		warning_msg += ":" + vars.PROXY_ADDRESS[1] + "' is unreachable."
		print stdout.warning_msg(warning_msg)

#load module on gui mode
if vars.GUI_MODE == True:
	helper.load_module(vars.MODULE)
else:
	#read console line arguments on console mode
	while True:
		try:
			input = raw_input(colors.fore_yellow(vars.PROGRAM + "_Framework> ", True))
			args = input.split(" ") #get command line arguments
				
			if args[0] == "exit":
				Style.RESET_ALL #stop console output coloring
				sys.exit(0)
			elif args[0] == "help":
				helper.commands_help()
			elif args[0] == "modules":
				helper.modules_list()
			elif args[0] == "sh":
				if len(args) > 1:
					Style.RESET_ALL
					cmd = misc.parse_args(args[1:])
					os.system(cmd)
			elif args[0] == "load":
				if len(args) == 2:
					helper.load_module(args[1])
			else:
				print colors.back_red("Unkown command. Type 'help' for commands help.")
		except KeyboardInterrupt:
			print Style.RESET_ALL #stop console output coloring and line break
