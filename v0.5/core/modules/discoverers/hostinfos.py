#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import pygeoip
from socket import *
from scapy.all import *

from core.lib import module
from core.lib import colors

from core.utils import vars
from core.utils import stdout

MODULE_NAME = "hostinfos"
DESCRIPTION = "Host informations grabber."
DESCRIPTION_FULL = "Grab host informations (IP address, hostname, GeoIP location)."

class HostInfos(module.Base):
	def __init__(self):
		self.module = MODULE_NAME
		
		if vars.GUI_MODE == False:
			print MODULE_NAME + "> " + DESCRIPTION_FULL
		
		if vars.TARGET != "":
			input = vars.TARGET
		else:
			input = raw_input("Target> ")
		
		if input != "":
			self.target = input
		else:
			error_msg = "Invalid target hostname or IP address."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		self.run()

	def run(self):
		info_msg = "Grabbing host informations"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
			
		content_msg = "Host state: "
		try:
			packet = IP(dst = gethostbyname(self.target), ttl = 20)/ICMP()
			response = sr1(packet, timeout = 3)
			if not response is None:
				content_msg += "Alive"
			else:
				content_msg += "Not alive"
			self.results += content_msg + "\n"
		except Exception, ex_msg:
			content_msg += str(ex_msg)
			if vars.LOGS:
				self.write_log(content_msg)
		print stdout.content_msg(content_msg)
				
		content_msg = "IP address: "
		try:
			content_msg += str(gethostbyname(self.target))
			self.results += content_msg + "\n"
		except Exception, ex_msg:
			content_msg += str(ex_msg)
			if vars.LOGS:
				self.write_log(content_msg)
		print stdout.content_msg(content_msg)
		
		content_msg = "Hostname: "
		try:
			content_msg += str(gethostbyaddr(self.target)[0])
			self.results += content_msg + "\n"
		except Exception, ex_msg:
			content_msg += str(ex_msg)
			if vars.LOGS:
				self.write_log(content_msg)
		print stdout.content_msg(content_msg)
		
		content_msg = "Location: "
		try:
			geoip = pygeoip.GeoIP(vars.GEOIP_FILE)
			record = geoip.record_by_addr(gethostbyname(self.target))
			content_msg += record['country_name']
			self.results += content_msg + "\n"
		except Exception, ex_msg:
			content_msg += str(ex_msg)
			if vars.LOGS:
				self.write_log(content_msg)
		print stdout.content_msg(content_msg)
		
		if vars.REPORT:
			self.write_report()
