#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

from socket import *

from core.lib import module
from core.lib import request
from core.lib import colors

from core.utils import vars
from core.utils import stdout

MODULE_NAME = "portscan"
DESCRIPTION = "Simple TCP port scanner."
DESCRIPTION_FULL = "Simple TCP port scanner. Scan for single port (e.g: 80), ports list (e.g: 80,21) or ports range (e.g: 0-65535)."

class PortScanner(module.Base):
	def __init__(self):
		self.module = MODULE_NAME
		
		if vars.GUI_MODE == False:
			print MODULE_NAME + "> " + DESCRIPTION_FULL
			
		if vars.TARGET != "":
			input = vars.TARGET
		else:
			input = raw_input("Target> ")
		
		if input != "":
			self.target = input
		else:
			error_msg = "Invalid target hostname or IP address."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		
		if vars.PORT != "":
			input = vars.PORT
		else:
			input = raw_input("Port(s)> ")
		
		if input != "":
			self.port = input
		else:
			error_msg = "Invalid port(s) number."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		self.run()
			
	def connect(self, port):
		try:
			service = str(getservbyport(int(port)))
		except:
			service = "unknown"
		try:
			socket = request.connect_socket(self.target, port)
			success_msg = port + "/" + service
			print stdout.success_msg(success_msg)
			self.results += success_msg + "\n"
			socket.disconnect()
		except:
			pass
		
	def run(self):
		info_msg = "Scanning for open port(s) '" + self.port + "'"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
			
		if "," not in self.port and "-" not in self.port:
			self.connect(self.port)
		elif "," in self.port:
			ports = self.port.split(",")
			for port in ports:
				self.connect(port)
		elif "-" in self.port:
			ports = self.port.split("-")
			for port in range(int(ports[0]), int(ports[1]) + 1):
				self.connect(str(port))
		
		if vars.REPORT:
			self.write_report()
