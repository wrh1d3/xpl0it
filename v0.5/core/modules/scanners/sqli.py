#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import re
import sys
import time

from core.lib import module
from core.lib import request
from core.lib import colors

from core.utils import vars
from core.utils import stdout
from core.utils import misc

from core.gui import messagebox

MODULE_NAME = "sqli"
DESCRIPTION = "SQL injection vulnerability scanner."
DESCRIPTION_FULL = "Scan for error based sql injection vulnerability."

#from sqliv.py 
dbs = {
	"MySQL": (r"SQL syntax.*MySQL", r"Warning.*mysql_.*", r"MySQL Query fail.*", r"SQL syntax.*MariaDB server"),
	"PostgreSQL": (r"PostgreSQL.*ERROR", r"Warning.*\Wpg_.*", r"Warning.*PostgreSQL"),
	"Microsoft SQL Server": (r"OLE DB.* SQL Server", r"(\W|\A)SQL Server.*Driver", r"Warning.*odbc_.*", r"Warning.*mssql_", r"Msg \d+, Level \d+, State \d+", r"Unclosed quotation mark after the character string", r"Microsoft OLE DB Provider for ODBC Drivers"),
	"Microsoft Access": (r"Microsoft Access Driver", r"Access Database Engine", r"Microsoft JET Database Engine", r".*Syntax error.*query expression"),
	"Oracle": (r"\bORA-[0-9][0-9][0-9][0-9]", r"Oracle error", r"Warning.*oci_.*", "Microsoft OLE DB Provider for Oracle"),
	"IBM DB2": (r"CLI Driver.*DB2", r"DB2 SQL error"),
	"SQLite": (r"SQLite/JDBCDriver", r"System.Data.SQLite.SQLiteException"),
	"Informix": (r"Warning.*ibase_.*", r"com.informix.jdbc"),
	"Sybase": (r"Warning.*sybase.*", r"Sybase message")
}

class SQLScanner(module.Base):
	def __init__(self):
		self.module = MODULE_NAME
		
		if vars.GUI_MODE == False:
			print MODULE_NAME + "> " + DESCRIPTION_FULL
			
		if vars.TARGET != "":
			input = vars.TARGET
		else:
			input = raw_input("Target> ")
		
		if input != "":
			self.target = input
		else:
			error_msg = "Invalid target hostname or IP address."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		self.run()

	def check_connection(self):
		info_msg = "Checking connection to '" + self.target + "'"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
			
		try:
			socket = request.connect_socket(self.target, 80)
			socket.disconnect()
			success_msg = "Connection established successfully."
			if vars.VERBOSE != 0:
				print stdout.success_msg(success_msg)
			if vars.LOGS:
				self.write_log(success_msg)
			return True
		except Exception, ex_msg:
			critical_msg = str(ex_msg)
			print stdout.critical_msg(critical_msg)
			if vars.LOGS:
				self.write_log(critical_msg)
			return False
		
	def inject(self, link, payloads):
		browser = request.connect_http()
		src = browser.open(link + "'").read()
		browser.close()
		for db_name, errors_msg in dbs.items():
			for error_msg in errors_msg:
				if re.compile(payload.strip("\n")).search(src): #from sqliv.py 
					return True, db
		return False, None

	def run(self):
		if not self.check_connection():
			return
			
		target = misc.check_url(self.target)
		if vars.USE_SSL:
			if not target.startswith("https"):
				target.replace("http", "https") #:p
					
		checking_msg = "Crawling internal links"
		if vars.VERBOSE != 0:
			print stdout.checking_msg(checking_msg)
		if vars.LOGS:
			self.write_log(checking_msg)
			
		browser = request.connect_http()
		browser.open(target)
		all_links = browser.links()
		internal_links = []
			
		for link in all_links:
			if link.url.startswith(target):
				internal_links.append(link.url)
			elif link.url.startswith("/"):
				internal_links.append(target + link.url)
			elif link.url.startswith("http://") or link.url.startswith("www.") or link.url.startswith("https://"):
				continue
			else:
				internal_links.append(target + "/" + link.url)
			
		if internal_links == []:
			critical_msg = "No internal link found."
			critical_msg += " Aborting procedure."
			print stdout.critical_msg(critical_msg)
			if vars.LOGS:
				self.write_log(critical_msg)
		else:
			info_msg = "Checking for vulnerable(s) link(s) to SQL injection"
			if vars.VERBOSE != 0:
				print stdout.info_msg(info_msg)
			if vars.LOGS:
				self.write_log(info_msg)
						
			links = []
			
			for link in internal_links:
				try:
					if "=" in link: #index.php?id=1
						if vars.VERBOSE != 0:
							checking_msg = "Testing " + link
							sys.stdout.write("\r" + stdout.checking_msg(checking_msg))
							sys.stdout.flush()
						success, db = self.inject(link)
						if success:
							links.append(link)
							success_msg = "Possible vulnerable link found '" + link + "'."
							success_msg += "  The database server name is '" + db + "'."
							print stdout.success_msg(success_msg)
							if len(links) == 1:
								if not vars.BATCH_MODE:
									if vars.GUI_MODE == False:
										question_msg = "Do you want to test others links (y/N) "
										input = raw_input(stdout.question_msg(question_msg))
									else:
										messagebox.question_msg("Do you want to test others links?")
										input = messagebox._user_choice
										if input in vars.NO_CHOICE:
											break
				except KeyboardInterrupt:
					error_msg = "Operation aborted, user pressed CTRL+C."
					print stdout.error_msg(error_msg)
					if vars.LOGS:
						self.write_log(error_msg)
			if len(links) == 0:
				failed_msg = "No vulnerable link to SQL injection found"
				print stdout.failed_msg(failed_msg)
				if vars.LOGS:
					self.write_log(failed_msg)
			else:
				for link in links:
					self.results += link + "\n"
					
			if vars.REPORT:
				self.write_report()
