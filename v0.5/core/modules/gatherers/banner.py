#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

from socket import *

from core.lib import module
from core.lib import request
from core.lib import colors

from core.utils import vars
from core.utils import stdout
from core.utils import misc

MODULE_NAME = "banner"
DESCRIPTION = "Server banner grabber."
DESCRIPTION_FULL = "Grab banner for single port (e.g: 80) or ports list (e.g: 80,21)."

class BannerGrabber(module.Base):
	def __init__(self):
		self.module = MODULE_NAME
		
		if vars.GUI_MODE == False:
			print MODULE_NAME + "> " + DESCRIPTION_FULL
			
		if vars.TARGET != "":
			input = vars.TARGET
		else:
			input = raw_input("Target> ")
			
		if input != "":
			self.target = input
		else:
			error_msg = "Invalid target hostname or IP address."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
			
		if vars.PORT != "":
			input = vars.PORT
		else:
			input = raw_input("Port(s)> ")
			
		if input != "":
			self.port = input
		else:
			error_msg = "Invalid port(s) number."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		self.run()
		
	def check_connection(self):
		info_msg = "Checking connection to '" + self.target + "'"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
			
		try:
			socket = request.connect_socket(self.target, 80)
			socket.disconnect()
			success_msg = "Connection established successfully."
			if vars.VERBOSE != 0:
				print stdout.success_msg(success_msg)
			if vars.LOGS:
				self.write_log(success_msg)
			return True
		except Exception, ex_msg:
			critical_msg = str(ex_msg)
			print stdout.critical_msg(critical_msg)
			if vars.LOGS:
				self.write_log(critical_msg)
			return False
		
	def connect(self, port):
		try:
			service = str(getservbyport(int(port)))
		except:
			service = "unknown"
		
		try:
			content_msg = port + "/" + service + "\n"
			if (int(port) == 80) or (int(port) == 8080):
				browser = request.connect_http()
				browser.open(misc.check_url(self.target))
				content_msg += str(browser.response().info())
				browser.close()
			else:
				socket = request.connect_socket(self.target, port)
				socket.send(vars.PROGRAM + "\r\n")
				content_msg += socket.recv(1024)
				socket.disconnect()
			print stdout.content_msg(content_msg)
			self.results += content_msg + "\n"
		except:
			pass
			
	def run(self):
		if not self.check_connection():
			return
		
		info_msg = "Grabbing server banner on port(s) '" + self.port + "'"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
			
		if "," not in self.port:
			self.connect(self.port)
		elif "," in self.port:
			ports = self.port.split(",")
			for port in ports:
				self.connect(str(port))
				
		if vars.REPORT:
			self.write_report()
