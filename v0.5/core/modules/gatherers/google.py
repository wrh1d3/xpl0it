#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import time
from bs4 import BeautifulSoup

from core.lib import module
from core.lib import request
from core.lib import colors

from core.utils import vars
from core.utils import stdout

MODULE_NAME = "google"
DESCRIPTION = "Google query."
DESCRIPTION_FULL = "Perform a google query. Set the maximum results count."

class GoogleSearch(module.Base):
	def __init__(self):
		self.module = MODULE_NAME
		
		if vars.GUI_MODE == False:
			print MODULE_NAME + "> " + DESCRIPTION_FULL
			
		if vars.QUERY != "":
			input = vars.QUERY
		else:
			input = raw_input("Query> ")
			
		if input != "":
			for query in input.split(" "):
				self.query += query + "+"
		else:
			error_msg = "Invalid query value."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
				
		if vars.MAX_RESULTS != "":
			input = int(vars.MAX_RESULTS)
		else:
			input = raw_input("Max results count> ")
			
		if input != "":
			try:
				self.max_count = int(input)
				if self.max_count < 10:
					self.max_count = 10
			except:
				self.max_count = 10
				warning_msg = "Invalid 'max_results' count." 
				warning_msg += " 'max_results' set default value to (" + str(self.max_count) + ")."
				print stdout.warning_msg(warning_msg)
				if vars.LOGS:
					self.write_log(warning_msg)
		else:
			error_msg = "Invalid maximum results count."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		self.run()
		
	def run(self):
		info_msg = "Querring google for '" + self.query + "'"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
				
		count = 0
		search_page = "https://www.google.com/search?hl=en&q="
		browser = request.connect_http()
		
		try:			
			while count < self.max_count:
				time.sleep(3)
				try:
					src = browser.open(search_page + str(self.query) + "&num=10&start=" + str(count)).read()
					#from https://stackoverflow.com/questions/33427504/how-can-i-scrape-the-first-link-of-a-google-search-with-beautiful-soup?rq=1
					soup = BeautifulSoup(src, "html.parser") 
					for link in soup.find_all("cite"):
						print stdout.content_msg(link.text)
						self.results += link.text + "\n"
					count += 10
				except KeyboardInterrupt:
					error_msg = "Operation aborted, user pressed CTRL+C."
					print stdout.error_msg(error_msg)
					if vars.LOGS:
						self.write_log(error_msg)
		except Exception, ex_msg:
			error_msg = str(ex_msg)
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
		finally:
			browser.close()
			
		if vars.REPORT:
			self.write_report()
