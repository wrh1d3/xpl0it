#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import shodan
from socket import *

from core.lib import module
from core.lib import colors

from core.utils import vars
from core.utils import stdout

MODULE_NAME = "shodanvuln"
DESCRIPTION = "Shodan vulnerability query."
DESCRIPTION_FULL = "Perform a vulnerability query on shodan database." 
DESCRIPTION_FULL += " WARNING: This module does not use any proxy settings."

class ShodanSearch(module.Base):
	def __init__(self):
		self.module = MODULE_NAME
		
		if vars.GUI_MODE == False:
			print MODULE_NAME + "> " + DESCRIPTION_FULL
			
		if vars.TARGET != "":
			input = vars.TARGET
		else:
			input = raw_input("Target> ")
			
		if input != "":
			self.target = input
		else:
			error_msg = "Invalid target hostname or IP address."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		
		if vars.API_KEY != "":
			input = vars.API_KEY
		else:
			input = raw_input("API key> ")
		
		if input != "":
			self.api_key = input
		else:
			error_msg = "Invalid shodan API key."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		self.run()

	def run(self):
		info_msg = "Querring shodan vulnerability database for '" + self.target + "'"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
		
		try:
			shodan_api = shodan.Shodan(self.api_key)
			host = shodan_api.host(str(gethostbyname(self.target)))
			
			for CVE in host['vulns']:
				vulns = "[" + CVE + "]\n"
				exploits = shodan_api.exploits.search(CVE.replace("!", ""))
				for item in exploits['matches']:
					if item.get('cve')[0] == CVE.replace("!", ""):
						vulns += item.get("description") + "\n"
				print stdout.resp_msg(vulns)
				self.results += vulns
		except KeyboardInterrupt:
			error_msg = "Operation aborted, user pressed CTRL+C."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
		except Exception, ex_msg:
			error_msg = str(ex_msg)
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			pass
			
		if vars.REPORT:
			self.write_report()
