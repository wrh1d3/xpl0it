xpl0it is a Web Penetration Testing Framework written in python and QT

Main features:

  - Discoverers (host informations)
  - Gatherers (banner grabber, google search, shodan vulnerability search)
  - Bruters (FTP, SMTP, HTTP)
  - Fuzzers (FTP username command)
  - Scanners (TCP port scanner, SQL vulnerability scanner)

Native system configuration:

  - Platform: Linux x86
  - Operating System: Xubuntu 18.04 LTS
  - Python version: 2.7.15rc1

Contributors:

  - AlthredTH (alfredth.com)

Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com
