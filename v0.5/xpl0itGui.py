#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This file is a part of xpl0it Web Penetration Testing Framework
#Copyright (c) 2016-2018 J3kill Soft. by wrh1d3 -> wrh1d3[at]gmail[dot]com

#This program is free, you can redistribute it and/or modify
#it according to respect of it's original author.

#See 'README.txt' for more informations. 

import os
import sys
import commands
from PyQt4 import QtGui
from PyQt4 import QtCore

from core.utils import requirements
from core.utils import vars
from core.utils import config

from core.gui import mainform
from core.gui import settingsform
from core.gui import aboutform
from core.gui import messagebox

from core.modules.discoverers import hostinfos
from core.modules.gatherers import google
from core.modules.gatherers import banner
from core.modules.gatherers import shodanvuln
from core.modules.scanners import portscan
from core.modules.scanners import sqli
from core.modules.fuzzers import ftpfuzz
from core.modules.bruters import ftpbrute
from core.modules.bruters import smtpbrute
from core.modules.bruters import httpbrute

vars.GUI_MODE = True
	
class ModuleThread(QtCore.QThread):
	def __init__(self):
		QtCore.QThread.__init__(self)
	
	def __del__(self):
		self.wait()
	
	def run(self):
		output = commands.getoutput("python ./xpl0it.py --config") #run console with --config option
		self.emit(QtCore.SIGNAL("show_output(QString)"), output)
		self.emit(QtCore.SIGNAL("show_message(QString)"), "Done!")
	
class MainWindow(QtGui.QMainWindow, mainform.Ui_MainWindow):
	def __init__(self):
		super(self.__class__, self).__init__()
		self.setupUi(self)
		self.center_window()

		self.actionSettings_2.triggered.connect(self.show_settings_form)
		self.actionDonate.triggered.connect(self.donate)
		self.actionAbout_2.triggered.connect(self.show_about_form)
		self.actionQuit_2.triggered.connect(self.close_window)
		self.comboBox.activated.connect(self.load_module)
		self.pushButton.clicked.connect(self.run_module)
		
	#https://bashelton.com/2009/06/pyqt-center-on-screen/ 
	def center_window(self):
		resolution = QtGui.QDesktopWidget().screenGeometry()
		self.move((resolution.width() / 2) - (self.frameSize().width() / 2),
			(resolution.height() / 2) - (self.frameSize().height() / 2))

	def show_settings_form(self):
		settings_form = settingsform.SettingsWindow()
		settings_form.exec_()
		
	def show_about_form(self):
		about_form = aboutform.AboutWindow()
		about_form.exec_()
		
	def donate(self):
		clipboard = QtGui.QApplication.clipboard()
		clipboard.setText("16kSwqthnKijpunzuVhru7x7N2AYvXnTdf", QtGui.QClipboard.Clipboard)
		messagebox.info_msg("Bitcoin address copied to clipboard!")

	def close_window(self):
		self.close()
		
	def run_module(self):
		vars.MODULE = str(self.comboBox.currentText())
		
		if vars.MODULE == "":
			messagebox.error_msg("No module selected.")
		else:
			#self.statusbar.showMessage("Running module, please wait...", 10000) #no way, this message doesn't appear or not at this point #wrh1d3
			messagebox.info_msg("Running module, please wait...")
			
			vars.TARGET = str(self.lineEdit.text())
			vars.QUERY = str(self.lineEdit.text())
			vars.USERNAME = str(self.lineEdit_2.text())
			vars.MAX_RESULTS = str(self.lineEdit_2.text())
			vars.API_KEY = str(self.lineEdit_2.text())
			
			if vars.MODULE == "ftpbrute" or vars.MODULE == "smtpbrute" or vars.MODULE == "ftpfuzz":
				vars.PORT = str(self.spinBox.Value())
			else:
				vars.PORT = str(self.lineEdit_2.text())
		
			#save configuration to file
			config.save_config()
			
			#start module thread
			module_thread = ModuleThread()
			self.connect(module_thread, QtCore.SIGNAL("show_output(QString)"), self.show_output)
			self.connect(module_thread, QtCore.SIGNAL("show_message(QString)"), self.show_message)
			module_thread.start()

	def show_output(self, output):
		self.plainTextEdit_2.setPlainText(output)
		
	def show_message(self, message):
		self.statusbar.showMessage(message, 10000)
	
	def load_module(self):
		if self.comboBox.currentIndex() == 1:
			self.plainTextEdit.setPlainText(hostinfos.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.hide()
			self.lineEdit_2.hide()
			self.groupBox_2.show()
		elif self.comboBox.currentIndex() == 2:
			self.plainTextEdit.setPlainText(banner.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.hide()
			self.lineEdit_2.show()
			self.lineEdit_2.setText("Port(s)")
			self.groupBox_2.show()
		elif self.comboBox.currentIndex() == 3:
			self.plainTextEdit.setPlainText(google.DESCRIPTION_FULL)
			self.lineEdit.setText("Query")
			self.spinBox.hide()
			self.lineEdit_2.show()
			self.lineEdit_2.setText("Max results count")
			self.groupBox_2.show()
		elif self.comboBox.currentIndex() == 4:
			self.plainTextEdit.setPlainText(shodanvuln.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.hide()
			self.lineEdit_2.show()
			self.lineEdit_2.setText("API key")
			self.groupBox_2.show()
		elif self.comboBox.currentIndex() == 5:
			self.plainTextEdit.setPlainText(portscan.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.hide()
			self.lineEdit_2.show()
			self.lineEdit_2.setText("Port(s)")
			self.groupBox_2.show()
		elif self.comboBox.currentIndex() == 6:
			self.plainTextEdit.setPlainText(sqli.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.hide()
			self.lineEdit_2.hide()
		elif self.comboBox.currentIndex() == 7:
			self.plainTextEdit.setPlainText(ftpfuzz.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.show()
			self.spinBox.setValue(21)
			self.lineEdit_2.hide()
			self.groupBox_2.show()
		elif self.comboBox.currentIndex() == 8:
			self.plainTextEdit.setPlainText(ftpbrute.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.show()
			self.spinBox.setValue(21)
			self.lineEdit_2.show()
			self.lineEdit_2.setText("Username")
			self.groupBox_2.show()
		elif self.comboBox.currentIndex() == 9:
			self.plainTextEdit.setPlainText(smtpbrute.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.show()
			self.spinBox.setValue(25)
			self.lineEdit_2.show()
			self.lineEdit_2.setText("Username")
			self.groupBox_2.show()
		elif self.comboBox.currentIndex() == 10:
			self.plainTextEdit.setPlainText(httpbrute.DESCRIPTION_FULL)
			self.lineEdit.setText("Target hostname or IP address")
			self.spinBox.hide()
			self.lineEdit_2.show()
			self.lineEdit_2.setText("Username")
			self.groupBox_2.show()
		else:
			self.plainTextEdit.setPlainText("")
			self.groupBox_2.hide()

if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	main_window = MainWindow()
	#main_window.setWindowFlags(QtCore.Qt.WindowCloseButtonHint|QtCore.Qt.WindowMinimizeButtonHint) I wanted to have only close and minimize buttons, but don't working #wrh1d3
	main_window.show()
	main_window.groupBox_2.hide()
	app.exec_()
